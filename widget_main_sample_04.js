/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples - Sample 04
 * 
 * Modify Widgets System messages (for Chat) via configuration of i18n messages in main widget.
 * Automatically inject a message, when a new chat session is started/connected.
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// Override widget configuration for i18n messages
window._genesys.widgets.main.i18n = {
    "en": {
        "webchat": {
            "ChatStarted": "Custom message on Chat Started - Please hold while we connect you",
            "ChatEnded": "Custom message on Chat Ended - The session has now ended.",
            "ChatTitle": "Custom Title - Chat",
            "ChatFormSubmit": "Custom - Start Chat"
        }
    }
};

// ##### Widget Define Local Customization

var _injectWelcomeInChatTranscript = function () {
    console.log('[Widgets] Injecting Welcome message in the Web Chat Transcript');

    localWidgetPlugin.command('WebChat.injectMessage', {
        type: 'text',
        name: 'ACME Contact Center',
        text: 'Custom Message - Thank you for contacting us! Someone will be with you shortly...',
        custom: true
    }).done(function (e) {
        // WebChat injected a message successfully
        // e.data == The message HTML reference (jQuery wrapped set)
    }).fail(function (e) {
        // WebChat isn't open or no actvite chat
    });
};


var initLocalCustomization = function () {

    if (localWidgetPlugin)
    {
        localWidgetPlugin.subscribe("WebChatService.clientConnected", function () {
            _injectWelcomeInChatTranscript();
        });
    }

};

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
    initLocalCustomization();
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});
