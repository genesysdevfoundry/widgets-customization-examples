# README #

Examples of customization with Genesys Widgets.

### Widgets Customization Examples ###

* Quick summary

These examples have been built using Genesys Widgets 8.5.005.35.
Updated: using up to Genesys Widgets 8.5.006.07.

Note that Genesys Widgets library and css are not provided in this repository, and will have to be installed separately.

### How do I get set up? ###

* Summary of set up

Get the source for the different examples in this repository.

Download the Genesys Widgets and copy the widgets.min.js into the \lib directory, and the widgets.min.css into the \stylesheets directory.
Genesys Widgets library and css are not provided in this repository.

* Configuration

At the top of the widget_init.js file, change the service URLs defined in the gmsServicesConfig object according to your environment (GMSChatURL, GMSEmailURL, GMSCallbackURL).

The WebChat, SendMessage, Callback and Channel Selector widgets are configured and enabled in the different samples.

Note that even if you don’t have a valid URL for Chat Service (or for Email/Message Service, or for Callback Service) – ex: service not configured in GMS, in your environment - you can still play with the widgets (open, close, prefill form, …).

* How to run tests

•	You can open the widget_index.html to navigate across the different customization examples, or open a sample page directly (ex: widget_sample_01.html).

### Available Customization Examples ###

* Sample 00

Base code and page, with Channel Selector, WebChat, SendMessage and Callback widgets configured.
Providing buttons to open WebChat, SendMessage, Callback and ChannelSelector.

* Sample 01

Custom buttons to open WebChat, SendMessage, and Callback with pre-filled data.

* Sample 02

Change (override) the chat, email and callback icons
In Channel Selector, Side Buttons and the forms (Chat, Message, Callback), using Custom CSS and Widgets configuration (Channel Selector).
Updated: using Genesys Widgets 8.5.006.07 (css changes).

* Sample 03

Automatically add a custom disclaimer (text and url) when the form is opened
In Channel Selector and in the different data forms (Chat, Message, Callback).

* Sample 04

Modify Widgets System messages (for Chat) via configuration of i18n messages in main widget.
Automatically inject a message, when a new chat session is started/connected.
 
* Sample 05

Show chat inactivity timeout messages on customer side
Using API Event subscription and API InjectMessage command.

* Sample 06

Pre-Fill v1 Form Widgets Extension.
This extension is primarily  meant to be called from Channel Selector.
WebChat, SendMessage, Callback are then requested via the custom extension, with parameters to automatically fill registration form with some custom data (WebChat/SendMessage/Callback open command).

* Sample 07

Pre-Fill v2 Form Widgets Extension.
This extension is primarily  meant to be called from Channel Selector.
WebChat, SendMessage, Callback are then requested via the custom extension, with parameters to automatically fill registration form with some custom data (WebChat/SendMessage/Callback open command).
Support for configure command added.

* Sample 08

Automatically fill registration form data (WebChat, SendMessage, Callback) when the form is opened, and automatically submit the form (if AutoSubmit is selected).
This is to provide a way to prefill registration form data, preserving the use of:
 - Chat and Message Side Buttons,
 - Automatic Chat Invite or Proactive Chat,
 - and Channel Selector (alternative to custom widget extension approach).

