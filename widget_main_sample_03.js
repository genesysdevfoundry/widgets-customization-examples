/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples - Sample 03
 * 
 * Automatically add a custom disclaimer (text and url) when the form is opened
 * In Channel Selector and in the different data forms (Chat, Message, Callback).
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// ##### Widget Define Local Customization

var _addDisclaimerInCXFooter = function () {
    console.log('[Widgets] Changing the CX Footer');
    // add line to cx footer
    $("div.cx-footer").append("<div>Here is a diclaimer at <a href='http://www.google.com'> this link</a></div>");
};

var initLocalCustomization = function () {

    if (localWidgetPlugin)
    {
        localWidgetPlugin.subscribe("WebChat.opened", function () {
            _addDisclaimerInCXFooter();
        });

        localWidgetPlugin.subscribe("SendMessage.opened", function () {
            _addDisclaimerInCXFooter();
        });

        localWidgetPlugin.subscribe("Callback.opened", function () {
            _addDisclaimerInCXFooter();
        });

        localWidgetPlugin.subscribe("ChannelSelector.opened", function () {
            _addDisclaimerInCXFooter();
        });
    }
};

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
    initLocalCustomization();
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});
