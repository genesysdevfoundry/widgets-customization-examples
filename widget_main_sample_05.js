/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples - Sample 05
 * 
 * Show chat inactivity timeout messages on customer side
 * Using API Event subscription and API InjectMessage command.
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// ##### Widget Define Local Customization

var _searchForChatInactivityInReceivedMessage = function (e) {
    console.log("XXX WebChatService.messageReceived: " + JSON.stringify(e));
    for (i = 0; i < Object.keys(e.data.messages).length; i++) {
        if (e.data.messages[i].type == "IdleAlert") {
            localWidgetPlugin.command("WebChat.injectMessage", {
                type: 'text',
                name: 'System',
                text: e.data.messages[i].text,
                custom: true
            });
        }
    }
};

var initLocalCustomization = function () {

    if (localWidgetPlugin)
    {
        localWidgetPlugin.subscribe("WebChatService.messageReceived", function (e) {
            _searchForChatInactivityInReceivedMessage(e);
        });
    }

};

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
    initLocalCustomization();
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});
