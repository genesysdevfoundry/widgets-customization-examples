/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples - Sample 02
 * 
 * Change (override) the chat, email and callback icons
 * In Channel Selector, Side Buttons and the forms (Chat, Message, Callback)
 * Using Custom CSS and Widgets configuration (Channel Selector)
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// Override widget configuration for Channel Selector media icons
window._genesys.widgets.channelselector.channels[0].icon = '';
window._genesys.widgets.channelselector.channels[0].html = '<img src=\'images/icons8-Callback-100.png\'>';
window._genesys.widgets.channelselector.channels[1].icon = '';
window._genesys.widgets.channelselector.channels[1].html = '<img src=\'images/icons8-Chat-100.png\'>';
window._genesys.widgets.channelselector.channels[2].icon = '';
window._genesys.widgets.channelselector.channels[2].html = '<img src=\'images/icons8-Message-100.png\'>';

// ##### Widget Define Local Customization

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-custom-styles",
            path: "stylesheets/mycustomcss_sample_02.css"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});




