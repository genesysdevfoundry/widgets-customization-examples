/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples - Sample 08
 * 
 * Automatically fill registration form data (WebChat, SendMessage, Callback) when the form is opened.
 * And automatically submit the form (if AutoSubmit is selected).
 * This is to provide a way to prefill registration form data, preserving the use of:
 * - Chat and Message Side Buttons,
 * - Automatic Chat Invite or Proactive Chat,
 * - and Channel Selector (alternative to custom widget extension approach).
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// ##### Widget Define Local Customization

var _prefillChatForm = function () {

    var lAutoSubmit = $('#PreChatSurvey #formAutoSubmit').is(':checked');

    console.log('[Widgets] prefilling the Web Chat Form');
    // custom form prefill
    $("#cx_webchat_form_firstname").val($('#PreChatSurvey #formFirstName').val());
    $("#cx_webchat_form_lastname").val($('#PreChatSurvey #formLastName').val());
    $("#cx_webchat_form_email").val($('#PreChatSurvey #formEmail').val());
    $("#cx_webchat_form_subject").val($('#PreChatSurvey #formSubject').val());

    if (lAutoSubmit == true)
    {
        // Widgets version up to 8.5.005.35
        $(".cx-webchat .cx-body .form .btn-primary.submit").click();
        // Widgets version 8.5.006.07 and after
        //$(".cx-webchat .cx-body .cx-form .cx-btn-primary.cx-submit").click();
		$(".cx-webchat .cx-btn-primary.cx-submit").click();
    }
};

var _prefillEmailForm = function () {

    var lAutoSubmit = $('#PreEmailSurvey #formAutoSubmit').is(':checked');

    console.log('[Widgets] prefilling the Email Form');
    // custom form prefill
    $("#cx_sendmessage_form_firstname").val($('#PreEmailSurvey #formFirstName').val());
    $("#cx_sendmessage_form_lastname").val($('#PreEmailSurvey #formLastName').val());
    $("#cx_sendmessage_form_email").val($('#PreEmailSurvey #formEmail').val());
    $("#cx_sendmessage_form_subject").val($('#PreEmailSurvey #formSubject').val());
    $("#cx_sendmessage_form_messagebody").val($('#PreEmailSurvey #formMessageBody').val());

    if (lAutoSubmit == true)
    {
        // Widgets version up to 8.5.005.35
        // Widgets version 8.5.006.07 and after
        localWidgetPlugin.command("SendMessage.submit");
    }
};

var _prefillCallbackForm = function () {

    var lAutoSubmit = $('#PreCallbackSurvey #formAutoSubmit').is(':checked');

    console.log('[Widgets] prefilling the Callback Form');
    // custom form prefill
    $("#cx_form_callback_firstname").val($('#PreCallbackSurvey #formFirstName').val());
    $("#cx_form_callback_lastname").val($('#PreCallbackSurvey #formLastName').val());
    $("#cx_form_callback_email").val($('#PreCallbackSurvey #formEmail').val());
    $("#cx_form_callback_subject").val($('#PreCallbackSurvey #formSubject').val());

    $("#cx_form_callback_phone_number").val($('#PreCallbackSurvey #formPhoneNumber').val());

    // now only, later date not supported
    if ($('#PreCallbackSurvey #formDesiredTime').val() == 'now')
        $("#cx_form_callback_now").prop('checked', true);

    // to force select a flag per country code (Phone Number) [Widgets version up to 8.5.005.35]
    //$("div.selected-flag").trigger('click');
    //$("div.flag-container ul.country-list li.country[data-country-code='es']").trigger('click');

    if (lAutoSubmit == true)
    {
        // Widgets version up to 8.5.005.35
        $(".cx-callback-container .cx-footer .btn-primary.cx-callback-confirm").click();
        // Widgets version 8.5.006.07 and after
        //$(".cx-callback .cx-button-container .cx-btn-primary.cx-callback-confirm").click();
		$(".cx-callback .cx-btn-primary.cx-callback-confirm").click();
    }
};

var initLocalCustomization = function () {

    if (localWidgetPlugin)
    {
        localWidgetPlugin.subscribe("WebChat.opened", function (e) {
            _prefillChatForm();
        });

        localWidgetPlugin.subscribe("SendMessage.opened", function (e) {
            _prefillEmailForm();
        });

        localWidgetPlugin.subscribe("Callback.opened", function (e) {
            _prefillCallbackForm();
        });
    }
};

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
    initLocalCustomization();
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});
