/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples - Sample 06
 * 
 * Pre-Fill v1 Form Widgets Extension.
 * This extension is primarily  meant to be called from Channel Selector.
 * WebChat, SendMessage, Callback are then requested via the custom extension,
 * with parameters to automatically fill registration form with some custom data
 * (WebChat/SendMessage/Callback open command).
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// Override widget configuration

//   Disable WebChat and SendMessage side buttons (no intercept to auto-fill the form in this example)
window._genesys.widgets.webchat.chatButton.enabled = false;
window._genesys.widgets.sendmessage.SendMessageButton.enabled = false;

//   Replace Callback, WebChat and SendMessage click commands, in Channel Selector, with Prefill widget extension commands
window._genesys.widgets.channelselector.channels[0].displayName = 'Receive a call (prefilled)';
window._genesys.widgets.channelselector.channels[0].clickCommand = 'Prefill.openCallback';
window._genesys.widgets.channelselector.channels[1].displayName = 'Web Chat (prefilled)';
window._genesys.widgets.channelselector.channels[1].clickCommand = 'Prefill.openWebChat';
window._genesys.widgets.channelselector.channels[2].displayName = 'Send Email (prefilled)';
window._genesys.widgets.channelselector.channels[2].clickCommand = 'Prefill.openSendMessage';


// ##### Widget Extension

if (!window._genesys.widgets.extensions) {
    window._genesys.widgets.extensions = {};
}

window._genesys.widgets.extensions["Prefill"] = function ($, CXBus, CXCommon)
{

    console.log('[Widgets Extension] Prefill - Loading');
    // Commands:
    // ---------
    // openWebChat
    // openSendMessage
    // openCallback

    // Events:
    // ---------
    // ready

    var oPrefill = CXBus.registerPlugin("Prefill");

    if (oPrefill)
    {

        /////////////////////////////////////////////////////////
        //
        //  Prefill Variables
        //
        /////////////////////////////////////////////////////////

        oPrefill.registerEvents(["ready"]);


        /////////////////////////////////////////////////////////
        //
        //  Prefill Commands
        //
        /////////////////////////////////////////////////////////

        oPrefill.registerCommand("openWebChat", function (e) {

            var pForm = {}, pUserData = {};

            if (e.data) {

                // TODO: your code to set your data into pForm and pUserData

                var lAutoSubmit = $('#PreChatSurvey #formAutoSubmit').is(':checked');
                pForm.autoSubmit = lAutoSubmit;
                pForm.firstname = $('#PreChatSurvey #formFirstName').val();
                pForm.lastname = $('#PreChatSurvey #formLastName').val();
                pForm.email = $('#PreChatSurvey #formEmail').val();
                pForm.subject = $('#PreChatSurvey #formSubject').val();

                oPrefill.command('WebChat.open', {
                    userData: pUserData,
                    form: pForm
                }).done(function (e) {
                    // WebChat opened successfully
                }).fail(function (e) {
                    // WebChat isn't opened
                });

                e.deferred.resolve();

            } else {

                e.deferred.reject("Invalid OpenWebChat");
            }
        });

        oPrefill.registerCommand("openSendMessage", function (e) {

            var pForm = {}, pUserData = {};

            if (e.data) {

                // TODO: your code to set your data into pForm and pUserData

                var lAutoSubmit = $('#PreEmailSurvey #formAutoSubmit').is(':checked');
                pForm.autoSubmit = lAutoSubmit;
                var lFormValidation = $('#PreEmailSurvey #formValidation').is(':checked');
                pForm.validation = lFormValidation;
                pForm.firstname = $('#PreEmailSurvey #formFirstName').val();
                pForm.lastname = $('#PreEmailSurvey #formLastName').val();
                pForm.email = $('#PreEmailSurvey #formEmail').val();
                pForm.subject = $('#PreEmailSurvey #formSubject').val();
                pForm.messagebody = $('#PreEmailSurvey #formMessageBody').val();

                oPrefill.command('SendMessage.open', {
                    userData: pUserData,
                    form: pForm
                }).done(function (e) {
                    // SendMessage opened successfully
                }).fail(function (e) {
                    // SendMessage isn't opened
                });

                e.deferred.resolve();

            } else {

                e.deferred.reject("Invalid OpenSendMessage");
            }
        });

        oPrefill.registerCommand("openCallback", function (e) {

            var pForm = {}, pUserData = {};

            if (e.data) {

                // TODO: your code to set your data into pForm and pUserData

                var lAutoSubmit = $('#PreCallbackSurvey #formAutoSubmit').is(':checked');
                pForm.autoSubmit = lAutoSubmit;

                pForm.firstname = $('#PreCallbackSurvey #formFirstName').val();
                pForm.lastname = $('#PreCallbackSurvey #formLastName').val();
                pForm.email = $('#PreCallbackSurvey #formEmail').val();
                pForm.subject = $('#PreCallbackSurvey #formSubject').val();
                pForm.phonenumber = $('#PreCallbackSurvey #formPhoneNumber').val();
                pForm.desiredTime = $('#PreCallbackSurvey #formDesiredTime').val();

                oPrefill.command('Callback.open', {
                    userData: pUserData,
                    form: pForm
                }).done(function (e) {
                    // Callback opened successfully
                }).fail(function (e) {
                    // Callback isn't opened
                });

                e.deferred.resolve();

            } else {

                e.deferred.reject("Invalid OpenCallback");
            }
        });

        /////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////
        //
        //  CX Widget Specific Subscriptions
        //
        /////////////////////////////////////////////////////////

        oPrefill.subscribe("App.closeAll", function (e) {

        });

        oPrefill.subscribe("App.ready", function (e) {

        });

        oPrefill.republish("ready");
        /////////////////////////////////////////////////////////

    }

};


// ##### Widget Define Local Customization

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});
