/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples - Sample 07
 * 
 * Pre-Fill v2 Form Widgets Extension.
 * This extension is primarily  meant to be called from Channel Selector.
 * WebChat, SendMessage, Callback are then requested via the custom extension,
 * with parameters to automatically fill registration form with some custom data
 * (WebChat/SendMessage/Callback open command).
 * Support for configure command added.
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// Override widget configuration

//   Disable WebChat and SendMessage side buttons (no intercept to auto-fill the form in this example)
window._genesys.widgets.webchat.chatButton.enabled = false;
window._genesys.widgets.sendmessage.SendMessageButton.enabled = false;

//   Replace Callback, WebChat and SendMessage click commands, in Channel Selector, with Prefill widget extension commands
window._genesys.widgets.channelselector.channels[0].displayName = 'Receive a call (prefilled)';
window._genesys.widgets.channelselector.channels[0].clickCommand = 'Prefill.openCallback';
window._genesys.widgets.channelselector.channels[1].displayName = 'Web Chat (prefilled)';
window._genesys.widgets.channelselector.channels[1].clickCommand = 'Prefill.openWebChat';
window._genesys.widgets.channelselector.channels[2].displayName = 'Send Email (prefilled)';
window._genesys.widgets.channelselector.channels[2].clickCommand = 'Prefill.openSendMessage';

//   Preconfigured data (if necessary)
window._genesys.widgets.prefill = {
    autoSubmit: false,
    form: {
        firstname: "Francois",
        lastname: "Dupont",
        email: "dupont@demo.demo",
        subject: "Dupont subject",
        messagebody: "Dupont Message Body",
        phonenumber: "+33123456789",
        desiredTime: "now"
    },
    userData: {
        CustomData1: "CustomValue1",
        CustomData2: "CustomValue2"
    }
};


// ##### Widget Extension


if (!window._genesys.widgets.extensions) {
    window._genesys.widgets.extensions = {};
}

window._genesys.widgets.extensions["Prefill"] = function ($, CXBus, CXCommon)
{

    console.log('[Widgets Extension] Prefill - Loading');
    // Commands:
    // ---------
    // openWebChat
    // openSendMessage
    // openCallback
    // configure

    // Events:
    // ---------
    // ready

    var oPrefill = CXBus.registerPlugin("Prefill");

    if (oPrefill)
    {

        /////////////////////////////////////////////////////////
        //
        //  Prefill Variables
        //
        /////////////////////////////////////////////////////////

        oPrefill.registerEvents(["ready"]);

        /* ---- Configuration Options ------------------- */

        var oConfig = {
            autoSubmit: false,
            form: {},
            userData: {}
        };

        /////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////
        //
        //  Prefill Commands
        //
        /////////////////////////////////////////////////////////

        oPrefill.registerCommand("openWebChat", function (e) {

            var pForm, pUserData, pAutoSubmit;

            if (e.data) {

                var cmdParameters = e.data;

                if ((typeof cmdParameters.form == "object") && (cmdParameters.form != {})) {
                    pForm = cmdParameters.form;
                } else {
                    if ((typeof oConfig.form == "object") && (oConfig.form != {})) {
                        pForm = oConfig.form;
                    } else {
                        pForm = {};
                    }
                }

                if ((typeof cmdParameters.userData == "object") && (cmdParameters.userData != {})) {
                    pUserData = cmdParameters.userData;
                } else {
                    if ((typeof oConfig.userData == "object") && (oConfig.userData != {})) {
                        pUserData = oConfig.userData;
                    } else {
                        pUserData = {};
                    }
                }

                if (typeof cmdParameters.autoSubmit == "boolean") {
                    pAutoSubmit = cmdParameters.autoSubmit;
                } else {
                    if (typeof oConfig.autoSubmit == "boolean") {
                        pAutoSubmit = oConfig.autoSubmit;
                    } else {
                        pAutoSubmit = false;
                    }
                }

                pForm.autoSubmit = pAutoSubmit;

                oPrefill.command('WebChat.open', {
                    userData: pUserData,
                    form: pForm
                }).done(function (e) {
                    // WebChat opened successfully
                }).fail(function (e) {
                    // WebChat isn't opened
                });

                e.deferred.resolve();

            } else {

                e.deferred.reject("Invalid OpenWebChat");
            }
        });

        oPrefill.registerCommand("openSendMessage", function (e) {

            var pForm, pUserData, pAutoSubmit;

            if (e.data) {

                var cmdParameters = e.data;

                if ((typeof cmdParameters.form == "object") && (cmdParameters.form != {})) {
                    pForm = cmdParameters.form;
                } else {
                    if ((typeof oConfig.form == "object") && (oConfig.form != {})) {
                        pForm = oConfig.form;
                    } else {
                        pForm = {};
                    }
                }

                if ((typeof cmdParameters.userData == "object") && (cmdParameters.userData != {})) {
                    pUserData = cmdParameters.userData;
                } else {
                    if ((typeof oConfig.userData == "object") && (oConfig.userData != {})) {
                        pUserData = oConfig.userData;
                    } else {
                        pUserData = {};
                    }
                }

                if (typeof cmdParameters.autoSubmit == "boolean") {
                    pAutoSubmit = cmdParameters.autoSubmit;
                } else {
                    if (typeof oConfig.autoSubmit == "boolean") {
                        pAutoSubmit = oConfig.autoSubmit;
                    } else {
                        pAutoSubmit = false;
                    }
                }

                pForm.autoSubmit = pAutoSubmit;

                oPrefill.command('SendMessage.open', {
                    userData: pUserData,
                    form: pForm
                }).done(function (e) {
                    // SendMessage opened successfully
                }).fail(function (e) {
                    // SendMessage isn't opened
                });

                e.deferred.resolve();

            } else {

                e.deferred.reject("Invalid OpenSendMessage");

            }
        });

        oPrefill.registerCommand("openCallback", function (e) {

            var pForm, pUserData, pAutoSubmit;

            if (e.data) {

                var cmdParameters = e.data;

                if ((typeof cmdParameters.form == "object") && (cmdParameters.form != {})) {
                    pForm = cmdParameters.form;
                } else {
                    if ((typeof oConfig.form == "object") && (oConfig.form != {})) {
                        pForm = oConfig.form;
                    } else {
                        pForm = {};
                    }
                }

                if ((typeof cmdParameters.userData == "object") && (cmdParameters.userData != {})) {
                    pUserData = cmdParameters.userData;
                } else {
                    if ((typeof oConfig.userData == "object") && (oConfig.userData != {})) {
                        pUserData = oConfig.userData;
                    } else {
                        pUserData = {};
                    }
                }

                if (typeof cmdParameters.autoSubmit == "boolean") {
                    pAutoSubmit = cmdParameters.autoSubmit;
                } else {
                    if (typeof oConfig.autoSubmit == "boolean") {
                        pAutoSubmit = oConfig.autoSubmit;
                    } else {
                        pAutoSubmit = false;
                    }
                }

                pForm.autoSubmit = pAutoSubmit;

                oPrefill.command('Callback.open', {
                    userData: pUserData,
                    form: pForm
                }).done(function (e) {
                    // Callback opened successfully
                }).fail(function (e) {
                    // Callback isn't opened
                });

                e.deferred.resolve();

            } else {

                e.deferred.reject("Invalid OpenCallback");

            }
        });

        oPrefill.registerCommand("configure", function (e) {

            if (e.data) {

                var cmdParameters = e.data;

                if ((typeof cmdParameters.form == "object") && (cmdParameters.form != {})) {
                    oConfig.form = cmdParameters.form;
                }

                if ((typeof cmdParameters.userData == "object") && (cmdParameters.userData != {})) {
                    oConfig.userData = cmdParameters.userData;
                }

                if (typeof cmdParameters.autoSubmit == "boolean") {
                    oConfig.autoSubmit = cmdParameters.autoSubmit;
                }

                e.deferred.resolve();
            } else {

                e.deferred.reject("Invalid configuration");
            }
        });
        /////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////
        //
        //  CX Widget Specific Subscriptions
        //
        /////////////////////////////////////////////////////////

        oPrefill.subscribe("App.closeAll", function (e) {

        });

        oPrefill.subscribe("App.ready", function (e) {

            if (e.data.prefill)
                oPrefill.command("configure", e.data.prefill);
            else
                oPrefill.command("configure", window._genesys.widgets.prefill);
        });

        oPrefill.republish("ready");
        /////////////////////////////////////////////////////////

    }

};


// ##### Widget Define Local Customization

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});
