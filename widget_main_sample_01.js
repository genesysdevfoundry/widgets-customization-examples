/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples - Sample 01
 * 
 * Custom buttons to open WebChat, SendMessage, and Callback with pre-filled data
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// ##### Widget Define Local Customization

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});
